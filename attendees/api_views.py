from django.http import JsonResponse

from .models import Attendee


def api_list_attendees(request, conference_id):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    attendees = Attendee.objects.filter(conference_id=conference_id)

    attendee_list = []
    for attendee in attendees:
        attendee_data = {
            "name": attendee.name,
            "href": f"/api/attendees/{attendee.id}/",
        }
        attendee_list.append(attendee_data)

    response_data = {
        "attendees": attendee_list
    }
    return JsonResponse(attendee_data)


def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    attendee = Attendee.objects.get(id=id)

    attendee_data = {
        "email": attendee.email,
        "name": attendee.name,
        "company_name": attendee.company_name,
        "created": attendee.created,
        "conference": {
            "name": attendee.conference.name,
            "href": f"/api/conferences/{attendee.conference_id}/",
        }
    }

    return JsonResponse(attendee_data)
